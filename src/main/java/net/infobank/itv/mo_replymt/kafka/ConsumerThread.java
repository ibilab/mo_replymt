package net.infobank.itv.mo_replymt.kafka;

import java.io.StringReader;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import net.infobank.itv.mo_common.model.ConsumerInfo;
import net.infobank.itv.mo_common.model.ProgramMsg;
import net.infobank.itv.mo_common.util.CommonUtils;
import net.infobank.itv.mo_replymt.model.ReplyMt;
import net.infobank.itv.mo_replymt.util.ReplyMtUtils;
import net.infobank.itv.mo_replymt.util.ScheduleUtils;

public class ConsumerThread implements Runnable {
    private static Logger log = LoggerFactory.getLogger(ConsumerThread.class);

    String BootStrapServer;
    ArrayList<String> Monum;
    String Pre_Groupname;
    int RefreshTimer;
    int ReLoad_Sec;
    int radioFlag;
    List<ReplyMt> replyDefault_list = new ArrayList<ReplyMt>();
    List<ReplyMt> replyFirst_list = new ArrayList<ReplyMt>();
    List<ReplyMt> replyKeyword_list = new ArrayList<ReplyMt>();
    
    // Sub Consumer
    KafkaConsumer<String, String> consumer = null;
    private Map<TopicPartition, OffsetAndMetadata> currentOffsets = new HashMap<>();

    private final AtomicBoolean running = new AtomicBoolean(false);

    public ConsumerThread(ConsumerInfo Info) {
        // store parameter for later user
        BootStrapServer = Info.getBoot_strap_server();
        Monum = Info.getMo_num();
        Pre_Groupname = Info.getPre_group_name();
        ReLoad_Sec = Info.getReload_sec();
        radioFlag = Info.getRadio_flag();
    }

    public void shutdown() {
        log.info("Stop this thread");
        running.set(false);
    }

    private class HandleRebalance implements ConsumerRebalanceListener {
        public void onPartitionsAssigned(Collection<TopicPartition> partitions) { 
        }

    	public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
    	    log.info("Lost partitions in rebalance. " +
    	            "Committing current offsets:" + currentOffsets);
    	    consumer.commitSync(currentOffsets);
    	 // Reblanacing error add 
    	    currentOffsets.clear(); // 추가 코드
	    }
	    
    }
    
	@Override
    public void run() {
        Thread shutDownHook = new ShutdownHook(Thread.currentThread(), "Shutdown");
        Runtime.getRuntime().addShutdownHook(shutDownHook);
        
        log.info("run thread : " + Thread.currentThread().getName());

        String group;
        String message = null;
        Gson g = new Gson();
        int GapTime = 0;
        int CurrentTime = 0;
        List<ProgramMsg> programMsg_list = new ArrayList<ProgramMsg>();
        List<ReplyMt> billingCode_list = new ArrayList<ReplyMt>();
        List<ReplyMt> replyMt_list = new ArrayList<ReplyMt>();

        // init Reload time
        CurrentTime = (int) (System.currentTimeMillis() / 1000);
        GapTime = CurrentTime;

        // 쓰레드 스탑
        running.set(true);
        
        
        // reply list 
        replyMt_list = ReplyMtUtils.getReplyMt();
        log.info(" replyMt_list :  " + replyMt_list.size());
        getReplydivide(replyMt_list);

        // sUB sETTING
        group = Pre_Groupname;
        log.info("groupname : " + group);

        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BootStrapServer);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, group);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        props.put(ConsumerConfig.ALLOW_AUTO_CREATE_TOPICS_CONFIG, false);
        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        consumer = new KafkaConsumer<String, String>(props);
        
        log.info(Monum.toString());

        
        try {
            consumer.subscribe(Monum, new HandleRebalance());
            //insertTopic(Monum);

        	while (running.get()) {
          	
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
                for (ConsumerRecord<String, String> record : records) {
                    try {
                        message = record.value();
                        
                        log.info("topic = {}, partition = {}, offset = {}, key = {}, msg = {}",
                                record.topic(), record.partition(), record.offset(),
                                record.key(), record.value());
                        
                        // Msg JSon 변환
                        JsonReader reader_body = new JsonReader(new StringReader(message));
                        reader_body.setLenient(true);
                        ProgramMsg body = g.fromJson(reader_body, ProgramMsg.class);
                        body.setMsg_userid(body.getMsg_username());
                        
                        // 게시판 처리 
                        programMsg_list.add(body);
                        
                        currentOffsets.put(
                                new TopicPartition(record.topic(), record.partition()),
                                new OffsetAndMetadata(record.offset()+1, null));

                    } catch(Exception e) {
                        log.error("Exception : ", e);
                    }
                 }
                
                // 벌크 처리 
                if(programMsg_list.size() > 0 ) { 
                	
                	Thread.sleep(500);
	                procReplyMt(programMsg_list,  radioFlag);
	                consumer.commitAsync(currentOffsets, null);
	                programMsg_list.clear();
                }
                

                // Schedule ReLoad
                CurrentTime = (int) (System.currentTimeMillis() / 1000);
                if (CurrentTime - GapTime >= ReLoad_Sec) {
                    GapTime = CurrentTime;
                    
                    ArrayList<String> mo_list = ScheduleUtils.getMOList();
                    if(!Arrays.equals(Monum.toArray(), mo_list.toArray())) {
                    	log.info(" Topic Change & Close & Re-Sub  ");
                    	consumer.subscribe(mo_list, new HandleRebalance());
                        // insertTopic(mo_list);
                    	Monum = mo_list;
                    }
                    
                    
                    // replylist get & divide
                    replyMt_list.clear();
                    replyMt_list = ReplyMtUtils.getReplyMt();
                    log.info(" replyMt_list :  " + replyMt_list.size());
                    getReplydivide(replyMt_list);
                    
                    
                    
                    // get billing code 
                    //billingCode_list.clear();
                    //billingCode_list = ReplyMtUtils.getReplyMt();
                    
                    // Mt Tmp Count User update
                    log.info(" Pre  UpdateMtUserCount :  ");
                    UpdateMtUserCount();
                    log.info(" Aft  UpdateMtUserCount :  ");
                }

        	} 
        } catch (WakeupException e) {
            // ignore, we're closing
        } catch(Exception e) {
            log.error(message, e);
        } finally {
            try {
                consumer.commitSync(currentOffsets);
            } finally {
                consumer.close();
    	        log.info("Exit thread : " + Thread.currentThread().getName());
            }
        }
    }
	
    private void procReplyMt(List<ProgramMsg> list, int radioFlag) {
        try {
        
        	List<ProgramMsg> mtList  = new ArrayList<ProgramMsg>();
        	
        	for( int i = 0 ; i < list.size() ; i++ )
            {
        		 ReplyMt replymt = null;
        		 String mtMsg = null;
        		 String mtSubject = "";
        		 
        		 //log.info(list.toString());
        		 int firstUser = 0;
        		 int nickUser = 0;
        		 
        		 //pgm_key == 0 이면 버려라 
        		 if(list.get(i).getPgm_key() == 0)
        			 continue;
        		 
        		 //문자만 자동 답장 적용         		 
        		 if(!list.get(i).getMsg_com().equals("001"))
        			 continue;
        		 
        		 //allow mt check 및 continue
        		 replymt = ReplyMtUtils.getAllowUser (list.get(i));
        		 
        		 if(replymt == null || replymt.getAllow_mt() == 1) {
        			 log.info(list.get(i).getMsg_userid() + " : is allowmt");
        		 } 
        		 else {
        			 log.info(list.get(i).getMsg_userid() + " : is not allowmt");
        			 continue;
        		 }

        		 
        		 //first check 및 msg_data 변경 및 continue  
        		 if(replymt == null || replymt.getMo_total() == 1) {
        		 //if(replymt == null ) {
        			 log.info(list.get(i).getMsg_userid() + " : is FirstUser");
        			 firstUser = 1;
        		 } 
        		 else {
        			 log.info(list.get(i).getMsg_userid() + " : is not FirstUser");
        		 }
        		 
        		 //키워드 있음 
   			 	 // allow Nick 변경 체크 msg_data 변경 및 continue
        		 if(radioFlag == 1 && "001".equals(list.get(i).getMsg_com())) { 
        			 // 닉 처리 MT
        			 
                     String nick = CommonUtils.findNick(list.get(i).getMsg_data());
                     if(nick.length() > 0) {
                         if(nick.getBytes().length > 32) nick = nick.substring(0, 32);
                         mtMsg = mtMsg.format("닉네임[%s]이 설정되었습니다.", nick);
                         log.info("NickName Setting Msg : " + mtMsg);
                         
                         nickUser = 1;
                     }
        		}
        		 
        		 // Last Msg Setting 
        		if(nickUser == 0) {
        			 
        			ReplyMt temp_replymt = new ReplyMt();
        			/// 처음 메시지 
        			if(firstUser == 1) {
        				temp_replymt = getFirstMsg(list.get(i).getPgm_key());
        				
        				if(temp_replymt != null) { 
	        				mtMsg= temp_replymt.getMt_msg();
	        				mtSubject= temp_replymt.getMt_subject();
	        				log.info("FirstUser mt Msg : " + mtSubject + "_"+ mtMsg);
        				}
        			}
        			
        			if(mtMsg == null) {
        				// keyword 처리 
        				temp_replymt = getKeywordMsg(list.get(i).getPgm_key(), list.get(i).getMsg_data());
        				if(temp_replymt != null) { 
	        				mtMsg= temp_replymt.getMt_msg();
	        				mtSubject= temp_replymt.getMt_subject();
	        				log.info("Mo Keyword mt Msg : " + mtSubject + "_"+ mtMsg);
        				}
        				else {
        					// 기본 메시지 처리 
            				temp_replymt = getDefaultMsg(list.get(i).getPgm_key());
            				
            				if(temp_replymt != null) { 
    	        				mtMsg= temp_replymt.getMt_msg();
    	        				mtSubject= temp_replymt.getMt_subject();
    	        				log.info("Default mt Msg : " + mtSubject + "_"+ mtMsg);
            				}
        				}
        				
        			}
        		}
        		 
    			
    			if(mtMsg == null) {
    				log.info("mt Msg 없음 : ");
    			}
    			else {
    				ReplyMt temp_replymt = new ReplyMt();
    				log.info("mt Msg : " + mtSubject + "_"+ mtMsg);
    				
    				
    				// callback 
    				list.get(i).setMsg_callback(list.get(i).getMsg_num() + list.get(i).getMsg_emo());
    				
    				// ref_key (br_key) 
    				temp_replymt =  ReplyMtUtils.selectBillingCode(list.get(i).getPgm_key());
    				list.get(i).setCh_key(temp_replymt.getCh_key());
    				list.get(i).setRef_key(temp_replymt.getBilling_code());
    				
    				// msg_data set
    				list.get(i).setMsg_data(mtMsg);
    				list.get(i).setMsg_title(mtSubject);
    				
    				mtList.add(list.get(i));
    			}
            	
            }

         	// mtList > 0 insert 
        	if(mtList.size() > 0) {
        		int ret = ReplyMtUtils.mtBulkInsert(mtList);
        		log.info("mtBulkInsert : " + ret);
        		
            	// mt count update        		
        		ReplyMtUtils.mtUpdateTmpCount(list);
        		
        		mtList.clear();
        	}
        	 
 
            
            

        } catch(Exception e) {
            e.printStackTrace();
        }
    }
	
    private void getReplydivide(List<ReplyMt> list) {
        try {
        	
        	//replyKeyword_list
        	replyDefault_list.clear();
        	replyFirst_list.clear();
        	replyKeyword_list.clear();
        	
            for( int i = 0 ; i < list.size() ; i++ )
            {
                if(list.get(i).getMo_word().equals("기본"))
                {
                	replyDefault_list.add(list.get(i));
                }
                else if(list.get(i).getMo_word().equals("처음"))
                {
                	replyFirst_list.add(list.get(i));
                }
                else
                {
                	replyKeyword_list.add(list.get(i));
                }
            }
            
            log.info("replyDefault_list : " + replyDefault_list.size());
            log.info("replyFirst_list : " + replyFirst_list.size());
            log.info("replyKeyword_list : " + replyKeyword_list.size());

        } catch(Exception e) {
            e.printStackTrace();
        }
    }    
    
    private ReplyMt getFirstMsg(int pgm_key) {
        
    	
    	try { 
	        List<ReplyMt> mtMsgList  = new ArrayList<ReplyMt>();
	        	 
			for (int i = 0; i < replyFirst_list.size(); i++) {
				
				if ( pgm_key ==  replyFirst_list.get(i).getPgm_key() ) {
					
					ReplyMt replymt = new ReplyMt();
					
					if(replyFirst_list.get(i).getMt_subject() == null)
						replymt.setMt_subject("");
					else 
						replymt.setMt_subject(replyFirst_list.get(i).getMt_subject());
					replymt.setMt_msg(replyFirst_list.get(i).getMt_msg());
					
					mtMsgList.add(replymt);
				}
			}
			
			if(mtMsgList.size() > 0) {
				
				int random = (int)(Math.random()*1000);
				int idx = random % mtMsgList.size();
	
				return mtMsgList.get(idx);			
			}
			
			
    	} catch(Exception e) {
            e.printStackTrace();
        }
    	
    	return null;
    }
    
  private ReplyMt getKeywordMsg(int pgm_key, String msg_data) {
        
	  try { 
	        List<ReplyMt> mtMsgList  = new ArrayList<ReplyMt>();
	        	 
			for (int i = 0; i < replyKeyword_list.size(); i++) {
				
				if ( pgm_key ==  replyKeyword_list.get(i).getPgm_key() && msg_data.contains(replyKeyword_list.get(i).getMo_word())) {
					
					ReplyMt replymt = new ReplyMt();
					
					if(replyKeyword_list.get(i).getMt_subject() == null)
						replymt.setMt_subject("");
					else 
						replymt.setMt_subject(replyKeyword_list.get(i).getMt_subject());
					replymt.setMt_msg(replyKeyword_list.get(i).getMt_msg());
					
					mtMsgList.add(replymt);
				}
			}
			
			if(mtMsgList.size() > 0) {
				
				int random = (int)(Math.random()*1000);
				int idx = random % mtMsgList.size();
	
				return mtMsgList.get(idx);			
			}
			
			
	  	} catch(Exception e) {
	  		e.printStackTrace();
	  	}
	  
	  	return null;
    }

  	private ReplyMt getDefaultMsg(int pgm_key) {
      
  		
  		try { 
	        List<ReplyMt> mtMsgList  = new ArrayList<ReplyMt>();
	      	 
			for (int i = 0; i < replyDefault_list.size(); i++) {
				
				if ( pgm_key ==  replyDefault_list.get(i).getPgm_key() ) {
					
					ReplyMt replymt = new ReplyMt();
					
					if(replyDefault_list.get(i).getMt_subject() == null)
						replymt.setMt_subject("");
					else 
						replymt.setMt_subject(replyDefault_list.get(i).getMt_subject());
					replymt.setMt_msg(replyDefault_list.get(i).getMt_msg());
					
					mtMsgList.add(replymt);
				}
			}
			
			if(mtMsgList.size() > 0) {
				
				int random = (int)(Math.random()*1000);
				int idx = random % mtMsgList.size();
	
				return mtMsgList.get(idx);			
			}
			
			
  		} catch(Exception e) {
  			e.printStackTrace();
  		}
  		
  		return null;
  	}
  	
  	
    private void UpdateMtUserCount() {
    	List<ReplyMt> list = new ArrayList<ReplyMt>();
    	try {
	    
	 
    		list = ReplyMtUtils.getMtTmpPgm();
    		
    		if(list.size() > 0 ) { 
	    		ReplyMtUtils.mtUpdateUserCount(list);
	    		ReplyMtUtils.mtDeleteTmpCount(list);
	    		
	    		log.info("Update & Delete Pgm_count : " + list.size());
    		}
	        
        } catch(Exception e) {
            e.printStackTrace();
        }

    }
    
    private class ShutdownHook extends Thread {
        private Thread target;
        
        public ShutdownHook(Thread target, String name) {
            super(name);
            this.target = target;
        }
        
        public void run() {
            shutdown();
            
            try {
                //target 쓰레드가 종료될 때 까지 기다린다.
                target.join();                                
            } catch (InterruptedException e) {
                log.error("sth wrong in shutdown process", e);
            }
        }
    }
}
