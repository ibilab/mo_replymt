package net.infobank.itv.mo_replymt.util;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_common.model.ProgramMsg;
import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;
import net.infobank.itv.mo_replymt.dao.ReplyMtDAO;
import net.infobank.itv.mo_replymt.model.ReplyMt;


public class ReplyMtUtils {
    private static Logger log = LoggerFactory.getLogger(ReplyMtUtils.class);
    

    
    public static List<ReplyMt> getReplyMt() {
    	ReplyMtDAO replymtDAO = new ReplyMtDAO(MyBatisConnectionFactory.getSqlSessionFactory());
    	
    	List<ReplyMt> list = new ArrayList<ReplyMt>();
        
        try {
        	list = replymtDAO.selectReplyMt();
        } catch (Exception e) {
            log.error("Exception getReplyMt : ", e);
        }
        return list;
    }
    
    public static List<ReplyMt> getReplyMt_2() {
    	ReplyMtDAO replymtDAO = new ReplyMtDAO(MyBatisConnectionFactory.getSqlSessionFactory());
    	
    	List<ReplyMt> list = new ArrayList<ReplyMt>();
        
        try {
        	list = replymtDAO.selectReplyMt_2();
        } catch (Exception e) {
            log.error("Exception getReplyMt_2 : ", e);
        }
        return list;
    }
    
    public static ReplyMt getAllowUser (ProgramMsg list) {
    	ReplyMt replymt = null;
    	ReplyMtDAO replymtDAO = new ReplyMtDAO(MyBatisConnectionFactory.getSqlSessionFactory());
        
        try {
        	replymt = replymtDAO.selectAllowUser(list);
        } catch (Exception e) {
            log.error("Exception getAllowUser : ", e);
        }
        return replymt;
    }
    
    public static ReplyMt getAllowUser_2 (ProgramMsg list) {
    	ReplyMt replymt = null;
    	ReplyMtDAO replymtDAO = new ReplyMtDAO(MyBatisConnectionFactory.getSqlSessionFactory());
        
        try {
        	replymt = replymtDAO.selectAllowUser_2(list);
        } catch (Exception e) {
            log.error("Exception getAllowUser_2 : ", e);
        }
        return replymt;
    }
    
    public static ReplyMt selectBillingCode (int pgm_key) {
    	ReplyMt replymt = null;
    	ReplyMtDAO replymtDAO = new ReplyMtDAO(MyBatisConnectionFactory.getSqlSessionFactory());
        
        try {
        	replymt = replymtDAO.selectBillingCode(pgm_key);
        } catch (Exception e) {
            log.error("Exception getAllowUser : ", e);
        }
        return replymt;
    }
    
    
    public static int mtBulkInsert(List<ProgramMsg> list) {
        int id = -1;
        ReplyMtDAO replymtDAO = new ReplyMtDAO(MyBatisConnectionFactory.getSqlSessionFactory());

        try {
        	id = replymtDAO.mtBulkInsert(list);
        } catch(Exception e)  {
            log.error("ReplyMt.mtbulkinsert : " , e);            
        }

        return id;
    }
    
    public static int mtBulkInsert_2(List<ProgramMsg> list) {
        int id = -1;
        ReplyMtDAO replymtDAO = new ReplyMtDAO(MyBatisConnectionFactory.getSqlSessionFactory());

        try {
        	id = replymtDAO.mtBulkInsert_2(list);
        } catch(Exception e)  {
            log.error("ReplyMt.mtbulkinsert_2 : " , e);            
        }

        return id;
    }
    
    
    public static int mtUpdateTmpCount(List<ProgramMsg> list) {
        int id = -1;
        ReplyMtDAO replymtDAO = new ReplyMtDAO(MyBatisConnectionFactory.getSqlSessionFactory());

        try {
        	id = replymtDAO.mtUpdateTmpCount(list);
        } catch(Exception e)  {
            log.error("ReplyMt.mtUpdateCount : " , e);            
        }

        return id;
    }
    
    public static int mtUpdateTmpCount_2(List<ProgramMsg> list) {
        int id = -1;
        ReplyMtDAO replymtDAO = new ReplyMtDAO(MyBatisConnectionFactory.getSqlSessionFactory());

        try {
        	id = replymtDAO.mtUpdateTmpCount_2(list);
        } catch(Exception e)  {
            log.error("ReplyMt.mtUpdateCount_2 : " , e);            
        }

        return id;
    }
    
    public static int mtUpdateUserCount(List<ReplyMt> list) {
        int id = -1;
        ReplyMtDAO replymtDAO = new ReplyMtDAO(MyBatisConnectionFactory.getSqlSessionFactory());

        try {
        	id = replymtDAO.mtUpdateUserCount(list);
        } catch(Exception e)  {
            log.error("ReplyMt.mtUpdateUserCount : " , e);            
        }

        return id;
    }
    
    public static int mtUpdateUserCount_2(List<ReplyMt> list) {
        int id = -1;
        ReplyMtDAO replymtDAO = new ReplyMtDAO(MyBatisConnectionFactory.getSqlSessionFactory());

        try {
        	id = replymtDAO.mtUpdateUserCount_2(list);
        } catch(Exception e)  {
            log.error("ReplyMt.mtUpdateUserCount_2 : " , e);            
        }

        return id;
    }
    
    public static List<ReplyMt> getMtTmpPgm() {
    	ReplyMtDAO replymtDAO = new ReplyMtDAO(MyBatisConnectionFactory.getSqlSessionFactory());
        List<ReplyMt> list = new ArrayList<ReplyMt>();
        try {
            list = replymtDAO.selectMtTmpPgm();
        } catch (Exception e) {
            log.error("Exception getMtTmpPgm : ", e);
        }
        return list;
    }
    
    public static int mtDeleteTmpCount(List<ReplyMt> list) {
        int id = -1;
        ReplyMtDAO replymtDAO = new ReplyMtDAO(MyBatisConnectionFactory.getSqlSessionFactory());

        try {
        	id = replymtDAO.mtDeleteTmpCount(list);
        } catch(Exception e)  {
            log.error("ReplyMt.mtUpdateUserCount : " , e);            
        }

        return id;
    }
    
    
    
}
