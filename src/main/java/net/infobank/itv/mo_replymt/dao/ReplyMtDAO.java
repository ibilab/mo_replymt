package net.infobank.itv.mo_replymt.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_common.model.ProgramMsg;
import net.infobank.itv.mo_replymt.model.ReplyMt;

public class ReplyMtDAO {
    private static Logger log = LoggerFactory.getLogger(ReplyMtDAO.class);
	private SqlSessionFactory sqlSessionFactory = null;

	public ReplyMtDAO(SqlSessionFactory sqlSessionFactory){
        this.sqlSessionFactory = sqlSessionFactory;
    }

	
	public List<ReplyMt> selectReplyMt() {
		List<ReplyMt> list = new ArrayList<ReplyMt>();
		SqlSession session = sqlSessionFactory.openSession();

		try {
			list = session.selectList("ReplyMt.selectReplyMt");
		} catch(Exception e)  {
		    log.error("ReplyMt.selectReplyMt : " , e);
		} finally {
			session.close();
		}

		return list;
	}
	
	public List<ReplyMt> selectReplyMt_2() {
		List<ReplyMt> list = new ArrayList<ReplyMt>();
		SqlSession session = sqlSessionFactory.openSession();

		try {
			list = session.selectList("ReplyMt.selectReplyMt_2");
		} catch(Exception e)  {
		    log.error("ReplyMt.selectReplyMt_2 : " , e);
		} finally {
			session.close();
		}

		return list;
	}
	
	public ReplyMt selectAllowUser(ProgramMsg list) {
		ReplyMt replymt = null;
		HashMap<String,String> map = new HashMap<String,String>();
		
		Integer pgm_key = list.getPgm_key();
		
		map.put("pgm_key", pgm_key.toString());
		map.put("user_id", list.getMsg_userid());
		map.put("msg_com", list.getMsg_com());
	
		
		SqlSession session = sqlSessionFactory.openSession();

		try {
			replymt = session.selectOne("ReplyMt.selectAllowUser", map);
			//log.info(allowMt.toString());
			
			if(replymt == null ) {
				log.info("FirstUser : " + list.getMsg_userid());
			}
		} catch(Exception e)  {
		    log.error("ReplyMt.selectAllowUser : " , e);
		} finally {
			session.close();
		}

		return replymt;
	}
	
	public ReplyMt selectAllowUser_2(ProgramMsg list) {
		ReplyMt replymt = null;
		HashMap<String,String> map = new HashMap<String,String>();
		
		Integer pgm_key = list.getPgm_key();
		
		map.put("pgm_key", pgm_key.toString());
		map.put("user_id", list.getMsg_userid());
		
		SqlSession session = sqlSessionFactory.openSession();

		try {
			replymt = session.selectOne("ReplyMt.selectAllowUser_2", map);
			//log.info(allowMt.toString());
			
			//log.info("=========" + replymt);
			
			if(replymt == null ) {
				log.info("FirstUser : " + list.getMsg_userid());
			}
		} catch(Exception e)  {
		    log.error("ReplyMt.selectAllowUser_2 : " , e);
		} finally {
			session.close();
		}

		return replymt;
	}
	
	public ReplyMt selectBillingCode(int pgm_key) {
		ReplyMt replymt = null;
		HashMap<String,String> map = new HashMap<String,String>();
		
		Integer to_pgm_key = pgm_key;
		map.put("pgm_key", to_pgm_key.toString());
		
		SqlSession session = sqlSessionFactory.openSession();

		try {
			replymt = session.selectOne("ReplyMt.selectBillingCode", map);
			//log.info(replymt.toString());
			
		} catch(Exception e)  {
		    log.error("ReplyMt.selectBillingCode : " , e);
		} finally {
			session.close();
		}
		return replymt;
	}
	
    public int mtBulkInsert(List<ProgramMsg> list) {
        int id = -1;
        SqlSession session = sqlSessionFactory.openSession();

        try {
        	
        	/*
        	Map<String, Object> insertMap = new HashMap<String, Object>();
        	insertMap.put("ProgramMsg", list);
        	*/
        	log.info(list.toString());
        
            id = session.insert("ReplyMt.mtBulkInsert", list);
            session.commit();
        } catch(Exception e)  {
            log.error("ReplyMt.bulkinsert : " , e);
            session.rollback();
        } finally {
            session.close();
        }

        return id;
    }
    
    public int mtBulkInsert_2(List<ProgramMsg> list) {
        int id = -1;
        SqlSession session = sqlSessionFactory.openSession();

        try {
        	
        	/*
        	Map<String, Object> insertMap = new HashMap<String, Object>();
        	insertMap.put("ProgramMsg", list);
        	*/
        	log.info(list.toString());
        
        	for(int i = 0; i < list.size() ; i++) {
        		
        		if(list.get(i).getMsg_data().length() < 81)
        			id = session.insert("ReplyMt.mtBulkInsert_2_sms", list.get(i));
        		else 
                    id = session.insert("ReplyMt.mtBulkInsert_2_lms", list.get(i));
        	}
            session.commit();
        } catch(Exception e)  {
            log.error("ReplyMt.bulkinsert_2 : " , e);
            session.rollback();
        } finally {
            session.close();
        }

        return id;
    }
    
    public int mtUpdateTmpCount(List<ProgramMsg> list) {
        int id = -1;
        SqlSession session = sqlSessionFactory.openSession();
        

        try {
        	log.info(list.toString());
        
        	for (int i = 0 ; i < list.size() ; i++) {
        		HashMap<String,String> map = new HashMap<String,String>();
        		Integer pgm_key = list.get(i).getPgm_key();
        		
        		map.put("pgm_key", pgm_key.toString());
        		map.put("msg_com", list.get(i).getMsg_com());
        		map.put("user_id", list.get(i).getMsg_userid());
        		
        		session.insert("ReplyMt.mtUpdateTmpCount", list.get(i));
        		session.commit();
        	}
        } catch(Exception e)  {
            log.error("ReplyMt.mtUpdateTmpCount : " , e);
            session.rollback();
        } finally {
            session.close();
        }

        return id;
    }
    
    public int mtUpdateTmpCount_2(List<ProgramMsg> list) {
        int id = -1;
        SqlSession session = sqlSessionFactory.openSession();
        

        try {
        	log.info(list.toString());
        
        	for (int i = 0 ; i < list.size() ; i++) {
        		HashMap<String,String> map = new HashMap<String,String>();
        		Integer pgm_key = list.get(i).getPgm_key();
        		
        		map.put("pgm_key", pgm_key.toString());
        		map.put("user_id", list.get(i).getMsg_userid());
        		
        		session.insert("ReplyMt.mtUpdateTmpCount_2", list.get(i));
        		session.commit();
        	}
        } catch(Exception e)  {
            log.error("ReplyMt.mtUpdateTmpCount_2 : " , e);
            session.rollback();
        } finally {
            session.close();
        }

        return id;
    }
    
    public int mtUpdateUserCount(List<ReplyMt> list) {
        int id = -1;
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
        	//log.info(list.toString());
        
        	for (int i = 0 ; i < list.size() ; i++) {

        		log.info(list.get(i).toString());
        		session.update("ReplyMt.mtUpdateUserCount", list.get(i));
        	}
        	session.commit();
        } catch(Exception e)  {
            log.error("ReplyMt.mtUpdateUserCount : " , e);
            session.rollback();
        } finally {
            session.close();
        }

        return id;
    }
    
    public int mtUpdateUserCount_2(List<ReplyMt> list) {
        int id = -1;
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
        	//log.info(list.toString());
        
        	for (int i = 0 ; i < list.size() ; i++) {

        		log.info(list.get(i).toString());
        		session.update("ReplyMt.mtUpdateUserCount_2", list.get(i));
        	}
        	session.commit();
        } catch(Exception e)  {
            log.error("ReplyMt.mtUpdateUserCount_2 : " , e);
            session.rollback();
        } finally {
            session.close();
        }

        return id;
    }
    
	public List<ReplyMt> selectMtTmpPgm() {
		List<ReplyMt> list = new ArrayList<ReplyMt>();
		SqlSession session = sqlSessionFactory.openSession();

		try {
			list = session.selectList("ReplyMt.selectMtTmpPgm");
			
		} catch(Exception e)  {
		    log.error("ReplyMt.selectMtTmpPgm : " , e);
		} finally {
			session.close();
		}

		return list;
	}
	
    public int mtDeleteTmpCount(List<ReplyMt> list) {
        int id = -1;
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
        	//log.info(list.toString());
        
        	for (int i = 0 ; i < list.size() ; i++) {

        		log.info(list.get(i).toString());
        		session.delete("ReplyMt.mtDeleteTmpCount", list.get(i));
        	}
        	session.commit();
        } catch(Exception e)  {
            log.error("ReplyMt.mtDeleteTmpCount : " , e);
            session.rollback();
        } finally {
            session.close();
        }

        return id;
    }


}
