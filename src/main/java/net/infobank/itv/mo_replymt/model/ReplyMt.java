package net.infobank.itv.mo_replymt.model;

import lombok.Data;

@Data
public class ReplyMt {

	private int keyword_idx;
	private int ch_key;
	private int pgm_key;
	private String billing_code;
	private String mt_subject;
	private String mo_word;
	private String mt_msg;
	private int allow_mt;
	private int mo_total;

}
